User.create!(name: "Stan",
             last_name: "Pavlovitsky",
             nickname: "spavlovitsky",
             email: "braileee@gmail.com",
             password: ENV['ADMIN_PASSWORD'],
             password_confirmation: ENV['ADMIN_PASSWORD'],
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name = Faker::Name.first_name
  nickname = Faker::Internet.user_name(%w(. _ -))
  email = "myfablebeat-#{n+1}@fablebeat.com"
  password = "password"
  User.create!(name: name,
               nickname: nickname,
               email: email,
               password: password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end

# Following relationships
users = User.all
user = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }