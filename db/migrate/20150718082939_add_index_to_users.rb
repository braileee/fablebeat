class AddIndexToUsers < ActiveRecord::Migration
  def change
    add_index(:users, [:email, :nickname], unique: true)
  end
end
