class ApplicationMailer < ActionMailer::Base
  default from: "noreply@fablebeat.com"
  layout 'mailer'
end
