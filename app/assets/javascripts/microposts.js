/*check the size of an image*/
var sizeCheck = function () {
    $('#micropost_picture').bind('change', function () {
        size_in_megabytes = this.files[0].size / 1024 / 1024;
        if (size_in_megabytes > 5) {
            alert('Maximum file size is 5MB. Please choose a smaller file.');
        }
    });
};
/*get the image name and push it to the name of the button*/
function getName(str) {
    if (str != '') {
        if (str.lastIndexOf('\\')) {
            var i = str.lastIndexOf('\\') + 1;
        }
        else {
            var i = str.lastIndexOf('/') + 1;
        }
        var filename = str.slice(i);
        console.log(filename);
        var uploaded = document.getElementById("picturename");
        console.log(uploaded);
        uploaded.innerHTML = filename;
    }
}

/*count characters in a post*/
var charsCount = function () {
    $('.post-button').attr('disabled', true);
    $('.post-button').addClass('disabled-button');
    $('.post-button').click(function () {
        $('.counter').text('140');
    });
    $('#micropost_content').keyup(function () {
        var postLength = $(this).val().length;
        var charactersLeft = 140 - postLength;
        $('.counter').text(charactersLeft);
        if (charactersLeft < 0 || charactersLeft == 140) {
            $('.post-button').attr('disabled', true);
            $('.post-button').addClass('disabled-button');
        }
        else {
            $('.post-button').attr('disabled', false);
            $('.post-button').removeClass('disabled-button');
        }
    });
};


$(document).ready(sizeCheck);
$(document).on('page:load', sizeCheck);
$(document).ready(charsCount);
$(document).on('page:load', charsCount);
