/*toggle menu*/
var main = function() {
    $('.icon-menu').click(function () {
        $('.menu').animate({
            left: "0px"
        }, 500);
    });
    $('.icon-close').click(function () {
        $('.menu').animate({
            left: "-285px"
        }, 300);
    })
};
$(document).ready(main);
$(document).on('page:load', main);