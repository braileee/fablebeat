//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require microposts
//= require users
jQuery( function($) {
    $('[data-toggle="tooltip"]').tooltip({ placement: "right" });
});
//= require_tree .