class SessionsController < ApplicationController

  def create
    #find user with the email of nickname
    user = User.find_by(email: params[:session][:email_nickname].downcase) ||
           User.find_by(nickname: params[:session][:email_nickname].downcase)
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'pages/home'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
