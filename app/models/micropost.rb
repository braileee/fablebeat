class Micropost < ActiveRecord::Base
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  has_attached_file :picture, :styles => { :large => "400x400" }
  # Validate content type
  validates_attachment_content_type :picture, :content_type => /\Aimage/

  # Validate filename
  validates_attachment_file_name :picture, :matches => [/png\Z/, /jpe?g\Z/]
  #validates_attachment :picture, :presence => true,
                       #:content_type => { :content_type => ["image/jpeg", "image/gif", "image/png", "image/jpg"] },
                       #:size => { :less_than => 5.megabytes }
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
end
