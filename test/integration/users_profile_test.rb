require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user = users(:stan)
  end

  test "profile display" do
    log_in_as(@user)
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.nickname)
    assert_match @user.followers.count.to_s, response.body
    assert_match @user.following.count.to_s, response.body
    assert_match @user.microposts.count.to_s, response.body
  end
end
