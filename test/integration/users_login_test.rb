require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:stan)
  end

  test "login with invalid information" do
    get root_path
    assert_template 'pages/home'
    post login_path, session: { email_nickname: "",
                                password: "" }
    assert_template 'pages/home'
    assert_not flash.empty?
  end

  test "login with valid information" do
    get root_path
    post login_path, session: { email_nickname: @user.email,
                                password: 'password' }
    assert is_logged_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template 'users/show'
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    #logout
    delete logout_path
    assert_not is_logged_in? #certain of deleted cookies
    assert_redirected_to root_url
  end

  test "login with remembering" do
    log_in_as(@user, remember_me: '1')
    assert_not_nil cookies['remember_token']
  end

  test "login without remembering" do
    log_in_as(@user, remember_me: '0')
    assert_nil cookies['remember_token']
  end
end