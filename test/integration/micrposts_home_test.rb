require 'test_helper'

class MicropostsHomeTest < ActionDispatch::IntegrationTest

  test "should redirect from microposts home page unless logged_in" do
    get home_path
    assert_redirected_to root_url
  end
end
