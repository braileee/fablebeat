require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def setup
    @user = User.new(name: "Stan", email: "stan@mymail.com",
                     nickname: "stanivanov", last_name: "Ivanov",
                     password: "foobar", password_confirmation: "foobar")
  end

  test "should be valid" do
    assert @user.valid?
  end

  #should be present: name, nickname, email
  test "name should be present" do
    @user.name = " "
    assert_not @user.valid?
  end

  test "nickname should be present" do
    @user.nickname = " "
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = " "
    assert_not @user.valid?
  end

  #shouldn't be too long: name, nickname, last_name < 51 symbols
  #email < 256 symbols
  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  test "last name should not be too long" do
    @user.last_name = "a" * 51
    assert_not @user.valid?
  end

  test "nickname should not be too long" do
    @user.nickname = "a" * 51
    assert_not @user.valid?
  end

  test "email should not be too long" do
    @user.email = "a" * 243 + "@somemail.com"
    assert_not @user.valid?
  end

  #shouldn't be too short
  test "name should not be too short" do
    @user.name = "a"
    assert_not @user.valid?
  end

  test "nickname should not be too short" do
    @user.nickname = "a"
    assert_not @user.valid?
  end

  #validations
  test "email validation should accept valid addresses" do
    valid_addresses = %w[ My-emaiL@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn ]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[ user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com examp@el..com foo@bar+baz.com ]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "nickname validation should pass" do
    valid_nicknames = %w[ john98 kevin95 super_star i-am-the-best
                        -0-hjg-fHfjk_ aHOl_489-dfd-AOl 078iojfkgf-_ ]
    valid_nicknames.each do |valid_nickname|
      @user.nickname = valid_nickname
      assert @user.valid?, "#{valid_nickname.inspect} should be valid"
    end
  end

  test "nickname validation should not pass" do
    invalid_nicknames = ["k*arl", "b&entl'", "^upper n:st(y)",
                         "k$iy@", "m*+orocco", "let:p" ,"John doe"]
    invalid_nicknames.each do |invalid_nickname|
      @user.nickname = invalid_nickname
      assert_not @user.valid?, "#{invalid_nickname.inspect} should not be valid"
    end
  end

  test "name validation should pass" do
    valid_names = %w[ sasha stAs mArgo KVEntin rOBERT ]
    valid_names.each do |valid_name|
      @user.name = valid_name
      assert @user.valid?, "#{valid_name.inspect} should be valid"
    end
  end

  test "name validation should not pass" do
    invalid_names = %w[ k-lArk j_oHn _Samant@ ^k!rk- s(a)sh(a)
                       k1rk 8tohn  ]
    invalid_names.each do |invalid_name|
      @user.name = invalid_name
      assert_not @user.valid?, "#{invalid_name.inspect} should not be valid"
    end
  end

  test "last name validation should pass" do
    valid_last_names = %w[ sMith Jobs kOOk iVANOV obama ]
    valid_last_names.each do |valid_last_name|
      @user.last_name = valid_last_name
      assert @user.valid?, "#{valid_last_name.inspect} should be valid"
    end
  end

  test "last name validation should not pass" do
    invalid_last_names = %w[ k-lArk j_oHn _Samant@ ^k!rk- s(a)sh(a)
                       k1rk 8tohn  ]
    invalid_last_names.each do |invalid_last_name|
      @user.last_name = invalid_last_name
      assert_not @user.valid?, "#{invalid_last_name.inspect} should not be valid"
    end
  end

  #unique: email, nickname
  test "email addresses should be unique" do
    duplicate_user = User.new(name: "John", last_name: "Doe",
                              email: @user.email.upcase , nickname: "johndoe",
                              password: "fortest", password_confirmation: "fortest")
    @user.save
    assert_not duplicate_user.valid?
  end

  test "nickname should be unique" do
    duplicate_user = User.new(name: "John", last_name: "Doe",
                              email:"johndoe@gmail.com" , nickname: @user.nickname.upcase)
    @user.save
    assert_not duplicate_user.valid?
  end

  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

  #password
  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 5
    assert_not @user.valid?
  end

  #user data should be normalized
  test "email should be downcased" do
    downcased_emails = %w[ fOOBAr@gMaiL.cOm tEsTiNGG@exAmple.com foRest_GUmp@examp.com ]
    downcased_emails.each do |downcased_email|
      @user.email = downcased_email
      @user.save
      assert @user.valid?, "#{downcased_email.inspect} should not be valid"
    end
  end

  test "nickname should be downcased" do
    downcased_nicknames = %w[ fOOBar foREST-GUMP cliNT_EASTwOOD DE12CAPRIo ]
    downcased_nicknames.each do |downcased_nickname|
      @user.nickname = downcased_nickname
      @user.save
      assert @user.valid?, "#{downcased_nickname.inspect} should not be valid"
    end
  end

  test "name should be capitalized" do
    downcased_names = %w[ joHNNy ShAWn mAtThew lIZA ]
    downcased_names.each do |downcased_name|
      @user.name = downcased_name
      @user.save
      assert @user.valid?, "#{downcased_name.inspect} should not be valid"
    end
  end

  test "last name should be capitalized" do
    downcased_last_names = %w[ dECApriO CONAuGHEY bAiLE  ]
    downcased_last_names.each do |downcased_last_name|
      @user.name = downcased_last_name
      @user.save
      assert @user.valid?, "#{downcased_last_name.inspect} should not be valid"
    end
  end

  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?(:remember, '')
  end

  test "associated microposts should be destroyed" do
    @user.save
    @user.microposts.create!(content: "Lorem ipsum")
    assert_difference 'Micropost.count', -1 do
      @user.destroy
    end
  end

  test "should follow and unfollow a user" do
    stan = users(:stan)
    jackie = users(:jackie)
    assert stan.following?(jackie)
    assert stan.followers.include?(jackie)
    stan.unfollow(jackie)
    assert_not stan.following?(jackie)
  end

  test "feed should have the right posts" do
    stan = users(:stan)
    jackie = users(:jackie)
    lana = users(:lana)
   # Posts from followed user
    lana.microposts.each do |post_following|
      assert stan.feed.include?(post_following)
    end
   # Posts from self
    stan.microposts.each do |post_self|
      assert stan.feed.include?(post_self)
    end
   # Posts from unfollowed user
    lana.microposts.each do |post_unfollowed|
      assert_not jackie.feed.include?(post_unfollowed)
    end
  end
end
