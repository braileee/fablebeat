require 'test_helper'

class PagesControllerTest < ActionController::TestCase
  def setup
    @main_title = "Fablebeat"
  end

  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Welcome | #{@main_title}"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About | #{@main_title}"
  end

  test "should get contact" do
    get :contact
    assert_response :success
    assert_select "title", "Contact | #{@main_title}"
  end
end
